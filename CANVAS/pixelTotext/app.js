const canvas = document.getElementById('canvas1');
const ctx = canvas.getContext('2d');

const image1 = new Image();
const init=(resolution)=>{
    ctx.clearRect(0,0,canvas.width,canvas.height)
    let effect;
    image1.src = 'homer.png';
    image1.onload = function(){
        canvas.width = image1.width;
        canvas.height = image1.height
        effect = new AsciiEffect(ctx,image1.width,image1.height)
         effect.draw(resolution)
       
    }
}

init(13);



class Cell{
    constructor(x,y,symbol,color){
        this.x =x
        this.y = y
        this.symbol = symbol;
        this.color = color;
    }
    draw(ctx){
         ctx.fillStyle = this.color;
         ctx.font = '15px Verdana';
         ctx.fillText(this.symbol,this.x,this.y) 
    }
}

class AsciiEffect{
    #imageCellArray = [];
    #symbols =[];
    #pixels = [];
    #ctx;
    #width;
    #height;

    constructor(ctx,width,height){
        this.#ctx = ctx;
        this.#width = width;
        this.#height = height;
        this.#ctx.drawImage(image1,0,0,this.#width,this.#height);
        this.#pixels = this.#ctx.getImageData(0,0,this.#width,this.#height);

    }

    #convertToSymbol(g){
        if (g>=250) return '¤';
        else if(g <=250 && g >= 240) return '$';
        else if (g<=240 && g >= 220) return '#';
        else if (g<=220 && g >= 200) return '+';
        else if (g<=200 && g >= 180) return '@';
        else if (g<=180 && g >= 160) return '&';
        else if (g<=160 && g >= 140) return '%';
        else if (g<=140 && g >= 120) return '-';
        else if (g<=120 && g >= 100) return '$';
        else if (g<=100 && g >= 80) return '_';
        else if (g<=80 && g >= 60) return '§';
        else if (g<=60 &&  g >= 40) return '!';
        else if (g<=40 && g >= 20) return 'o';
        else if (g<20) return '¤';
        
    }

    #scanImage(cellSize){
        this.#imageCellArray =[];
        for(let y=0;y <this.#pixels.height;y += cellSize){
            for(let x=0;x<this.#pixels.width;x += cellSize){
                const posX = x*4;
                const posY = y*4;
                const pos = (posY * this.#pixels.width)+ posX;
                if(this.#pixels.data[pos +3] > 128){
                    const red = this.#pixels.data[pos];
                    const green = this.#pixels.data[pos + 1];
                    const blue = this.#pixels.data[pos + 2];
                    const total = red+green+blue;
                    const averageColorValue = total/3;
                    const color = "rgb("+red+","+green+","+blue+")";
                    const symbol = this.#convertToSymbol(averageColorValue);
                   if(total > 360) this.#imageCellArray.push(new Cell(x,y,symbol,color));
                }
            }
        }
        console.log(this.#imageCellArray)
    }

    #drawAscii(){
        this.#ctx.clearRect(0,0,this.#width, this.#height)
        for(let i =0; i<this.#imageCellArray.length;i++){
            this.#imageCellArray[i].draw(this.#ctx)
        }
    }

    draw(cellSize){
        this.#scanImage(cellSize);
        this.#drawAscii();
    }


}

