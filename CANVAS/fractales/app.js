
    const canvas = document.getElementById('canvas1');
    const ctx = canvas.getContext('2d');
    canvas.width = innerWidth;
    canvas.height = innerHeight;

    
    ctx.lineWidth=5
    ctx.lineCap='round';
    ctx.shadowColor ='rgba(0,0,0,0.7)';
    ctx.shadowOffsetX =10;
    ctx.shadowOffsetY =5;
    ctx.shadowBlur=10
    let r = Math.random()*255;
    let g = Math.random()*255;
    let b = Math.random()*255;
    let color = `rgb(${r},${g},${b})`;

    let sides =document.querySelector('#numberSides').value;
    let spread =document.querySelector('#numberSpread').value;
    let scale =document.querySelector('#numberScale').value;
    let branches =document.querySelector('#numberBranches').value;
    let depthMax =  document.querySelector('#numberMaxDepth').value;

    document.querySelector('#sidesValue').innerText =sides;

    //effect setting
    let size=innerHeight/4;
    
    function updateFractale(){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        sides = document.querySelector('#numberSides').value;
        spread = document.querySelector('#numberSpread').value;
        branches = document.querySelector('#numberBranches').value;
        scale = document.querySelector('#numberScale').value;
        depthMax = document.querySelector('#numberMaxDepth').value;
        drawFractal();


        document.querySelector('#sidesValue').innerText =sides;

    }

    

   
   




    function drawBranch(deep = 0){
        if(deep >depthMax)return;
        ctx.beginPath();
        ctx.moveTo(0,0);
        ctx.lineTo(size,0)
        ctx.stroke();

        for(let i=0;i<branches;i++){
            ctx.save()
            ctx.translate(size-(size/branches)*i,0)
            ctx.rotate(spread)
            ctx.scale(scale,scale)
            drawBranch(deep+1)
            ctx.restore();

            ctx.save()
            ctx.translate(size-(size/branches)*i,0)
            ctx.rotate(-spread)
            ctx.scale(scale,scale)
            drawBranch(deep+1)
            ctx.restore();

        }

        
    }
    

    function drawFractal(){
        ctx.save()
        ctx.strokeStyle=color;
        ctx.translate(canvas.width/2,canvas.height/2)
        
        for(i=0;i<sides;i++){
            ctx.rotate((Math.PI *2)/sides)
            drawBranch()
        }
        ctx.restore();
    }

    drawFractal();
