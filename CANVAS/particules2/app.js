let canvas, ctx, w, h, particles = [], hue = 0;
let particleCount = 200,
		particleChance = 0.8;
let mouse = {
	x: undefined,
	y: undefined
}

function init() {
	canvas = document.querySelector("#canvas");
	ctx = canvas.getContext("2d");

	resizeReset();
	animationLoop();
}

function resizeReset() {
	w = canvas.width = innerWidth;
	h = canvas.height = innerHeight;

	ctx.fillStyle = "#222222";
	ctx.fillRect(0, 0, w, h);
}

function mousemove(e) {
	mouse.x = e.x;
	mouse.y = e.y;
}

function mouseout() {
	mouse.x = undefined;
	mouse.y = undefined;
}

function animationLoop() {
	if (particles.length < particleCount && Math.random() < particleChance) {
		particles.push(new Particle());
	}

	ctx.globalCompositeOperation = "source-over";
	ctx.fillStyle = "rgba(0, 0, 0, .1)";
	ctx.fillRect(0, 0, w, h);
	ctx.globalCompositeOperation = "lighter";

	drawScene();
	requestAnimationFrame(animationLoop);
}

function drawScene() {
	particles.map((particle) => {
		particle.update();
		particle.draw();
	});
}

function getRandomInt(min, max) {
	return Math.round(Math.random() * (max - min)) + min;
}

function easeInSine(x) {
  return 1 - Math.cos((x * Math.PI) / 2);
}

function easeInExpo(x) {
	return x === 0 ? 0 : Math.pow(2, 10 * x - 10);
}

function getAngle(x1, y1, x2, y2) {
	let rad = Math.atan2(x2 - x1, y2 - y1);
	return (rad * 180) / Math.PI;
}

class Particle {
	constructor() {
		this.reset();
		this.constructed = true;
	}
	reset() {
		this.x = Math.random() * (w / 2) + (w / 4);
		this.y = h / 2;
		this.size = getRandomInt(5, 10);
		this.baseSize = this.size;

		this.angle = 45;
		this.dx = ((w / 2) - this.x) / (w / 4);
		if (this.dx >= 0) {
			if (getRandomInt(0, 1)) {
				this.angle = this.dx * 90 + 180;
			} else {
				this.angle = 360 - this.dx * 90;
			}
		} else if (this.dx < 0) {
			if (getRandomInt(0, 1)) {
				this.angle = -this.dx * 90;
			} else {
				this.angle = 180 - (-this.dx * 90);
			}
		}

		this.speed = getRandomInt(10, 20) / 10;

		this.force = Math.abs(Math.abs(this.dx) - 0.5) * 2;

		this.time = 0;
		this.ttl = 60 + (easeInExpo(this.force) * 90);

		this.hue = hue;
		hue += 1;
	}
	draw() {
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.size, 0, Math.PI * 2);
		ctx.fillStyle = `hsla(${this.hue}, 80%, 40%, 1)`;
		ctx.fill();
		ctx.closePath();
	}
	update() {
		let progress = 1 - (this.ttl - this.time) / this.ttl;

		if (mouse.x) {
			this.angle = getAngle(this.x, this.y, mouse.x, mouse.y);
		}

		this.radian = (Math.PI / 180) * this.angle;

		this.x += this.speed * Math.sin(this.radian);
		this.y += this.speed * Math.cos(this.radian);
		this.size = this.baseSize - (this.baseSize * easeInSine(progress));

		this.time++;

		if (progress > 1) this.reset();
	}
}

window.addEventListener("DOMContentLoaded", init);
window.addEventListener("resize", resizeReset);
window.addEventListener("mousemove", mousemove);
window.addEventListener("mouseout", mouseout);