/* eslint-disable */
import * as L from 'leaflet';
import {LeafletLayer} from 'deck.gl-leaflet';
import {MapView} from '@deck.gl/core';
import {GeoJsonLayer} from '@deck.gl/layers';
import {DataFilterExtension} from '@deck.gl/extensions';

// source: Natural Earth http://www.naturalearthdata.com/ via geojson.xyz
const PLUDATA = 'plu.geojson';

const map = L.map(document.getElementById('map'), {
  center: [50.62925, 3.057256],
  zoom: 12
});
L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png', {
	maxZoom: 20,
	attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
}).addTo(map);

const getPluColor=(f)=>{

    switch(f.properties.LIBELLE){
        case 'A': return [247,253,247]
        case 'AUCM': return [253,253,162]
        case 'AUCA': return [253,253,162]
        case 'AUCMZ13': return [253,253,162]
    
        case 'N': return [226,253,175]
        case 'NE': return [202,252,232]
        case 'NL': return [234,246,120]


        case 'UCA2.1':return [248,206,121]
        case 'UCA2.1.1':return [248,206,121]
        case 'UCA1.1':return [248,206,121]

        case 'UCA3.1':return [248,233,121]
        case 'UCA4.1':return [248,233,121]
        case 'UCA5.1':return [248,233,121]
        case 'UCO5.1':return [248,233,121]
        case 'UGB5.1':return [248,233,121]
        case 'UVC5.1':return [248,233,121]
         
        case 'UAR4.2':return [248,233,121]
        case 'UI': return [244,229,248]
        case 'UVD1.1': return [248,206,121]
        case 'UVD6.1': return [248,239,205]
        case 'UX':  return [239,214,235]

        case 'UZ1.1':  return [238,243,245]
        case 'UZ1.2':  return [238,243,245]
        case 'UZ1.3':  return [238,243,245]
        case 'UZ1.4':  return [238,243,245]
        case 'UZ4.1':  return [238,243,245]
        case 'UZ4.2':  return [238,243,245]
        case 'UZ12.1':  return [238,243,245]
        case 'UZ12.2':  return [238,243,245]
   
        default:  return [0,0,0]
    }
   }


const PLUlayer = new GeoJsonLayer({
   
      id: 'airports',
      data: PLUDATA,
      // Styles
     filled:true,
      getLineWidth:7,
      getLineColor:[0,0,0],
      pickable:true,
      autoHighlight: true,
     
      opacity:0.6,
     // onHover: (info, event) => console.log('Hovered:', info),
      getFillColor: info=>getPluColor(info) ,
      

    })
   


const deckLayer = new LeafletLayer({
    views: [
        new MapView({
        repeat: true
        })
    ],
    layers:[PLUlayer],
    getTooltip: ({object}) =>  
        object && ({
            html: `${object.properties.LIBELLE}`,
            style: {
                backgroundColor: '#000',
                fontSize: '1.2em'
            }
        })
});    
map.addLayer(deckLayer);

const featureGroup = L.featureGroup();

map.addLayer(featureGroup);
