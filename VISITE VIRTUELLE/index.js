function show(id,src) {

  document.getElementById('sky').setAttribute('src', src);
  
  let elementHide = document.querySelectorAll(".element");
  elementHide.forEach(function(item) {
    item.setAttribute("visible", "false");
  });
  
  let element = document.querySelectorAll("."+id);
  element.forEach(function(item) {
    item.setAttribute("visible", "true");
  });
}

const toggleVisibility = (element)=>{
  
  let elementList = document.querySelectorAll(element);
  elementList.forEach(function(item) {
    if (window.getComputedStyle(item).display === 'block') {
      item.style.display = 'none';
      return;
    }
    item.style.display = 'block';
  });
}


const getMenu = ()=>{
  toggleVisibility('#menuPano');

}

// const getDataElement = ()=>{
//   console.log(this);
// }

const createAsset = (ensemble)=>{
  
  const pano = panoramas.filter(elmt => { 
    return elmt.ensemble == ensemble
   });

   const elements = Listelements.filter(elmt => { 
    return elmt.ensemble == ensemble
   });
   let scene = document.querySelector('#SceneOne');
   elements.forEach((item)=>{

    let sceneElement = document.createElement("a-sphere");
    sceneElement.setAttribute("position", item.position);
    sceneElement.setAttribute("color", "yellow");
   
    sceneElement.setAttribute("radius", "0.2");
    sceneElement.setAttribute("click-log",'');
    sceneElement.setAttribute("visible", false);
    sceneElement.setAttribute("id", item.id);
    sceneElement.setAttribute("class","element "+item.pano);
    scene.appendChild(sceneElement);
   

 })
   carrousel ="";
   
   pano.forEach((item)=>{
      if(item.default === true){
        show(item.id,item.src)
       
      }
      
      carrousel +='<div class="container">';
      carrousel +='<img src="'+item.src+'"  class="miniature">';
      carrousel +='<div class="overlay" onclick="show(\''+item.id+'\',\''+item.src+'\')"> <div class="text" >'+item.name+'</div></div>';
      carrousel +='</div>';

   })
   
   document.querySelector('#menuVisite').innerHTML=carrousel;
  

}

const createPano=()=>{
  let menuEnsemble =[]
  const ensembles = panoramas.filter(elmt => { 
    menuEnsemble.push(elmt.ensemble)
    
   });
  menuEnsemble = new Set(menuEnsemble)
  let menuPano = document.querySelector('#menuPano');
  menuEnsemble.forEach(ensemble=>{
    let panoElement = document.createElement("section");
    panoElement.innerText = ensemble;
    panoElement.setAttribute("onclick","createAsset('"+ensemble+"')")
    menuPano.appendChild(panoElement)
   })

}

createAsset('Extérieur');
createPano();




