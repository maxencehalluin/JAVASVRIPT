const panoramas=[

    {ensemble:'Extérieur',name:'Extérieur 1',id:'a1',src:'assets/Phalecque2021_Ext_001.jpg',default:true},
    {ensemble:'Extérieur',name:'Extérieur 2',id:'a2',src:'assets/Phalecque2021_Ext_002.jpg',default:false},
    {ensemble:'Extérieur',name:'Extérieur 3',id:'a3',src:'assets/Phalecque2021_Ext_003.jpg',default:false},
    {ensemble:'Extérieur',name:'Extérieur 4',id:'a4',src:'assets/Phalecque2021_Ext_004.jpg',default:false},
    {ensemble:'Extérieur',name:'Extérieur 5',id:'a5',src:'assets/Phalecque2021_Ext_005.jpg',default:false},
    {ensemble:'Extérieur',name:'Extérieur 6',id:'a6',src:'assets/Phalecque2021_Ext_006.jpg',default:false},
    {ensemble:'Extérieur',name:'Extérieur 7',id:'a7',src:'assets/Phalecque2021_Ext_007.jpg',default:false},
    {ensemble:'Extérieur',name:'Extérieur 8',id:'a8',src:'assets/Phalecque2021_Ext_008.jpg',default:false},
    {ensemble:'Extérieur',name:'Extérieur 9',id:'a9',src:'assets/Phalecque2021_Ext_009.jpg',default:false},
    {ensemble:'Extérieur',name:'Extérieur 10',id:'a10',src:'assets/Phalecque2021_Ext_010.jpg',default:false},

    {ensemble:'Rdc',name:'Rdc 01',id:'rdc01',src:'assets/Phalecque2021_Ext_010.jpg'}
]